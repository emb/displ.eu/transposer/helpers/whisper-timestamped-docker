import uvicorn

from fastapi import FastAPI
import torch

app = FastAPI()

@app.get("/check_cuda/")
def check_cuda():
    if torch.cuda.is_available():
        print("CUDA is available on this system.")
        device = torch.cuda.get_device_name(0)
        return "Current GPU device:" + device
    else:
        return "CUDA is not available on this system. Using CPU."


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)