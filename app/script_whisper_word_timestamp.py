import whisper_timestamped as whisper
import timeit
import json
import sys

def audio_transcribe(file_path: str, model_ver: str = "base", lang: str = "de"):
    # Print the progress
    print('Start transcribing for:', file_path)
    start = timeit.default_timer()
    try:
        file_full_name = file_path.split("/")[-1]
        file_name = file_full_name.split(".")[0]

        # Get file name without extension
        audio = whisper.load_audio(file_path)
        model = whisper.load_model(model_ver, device="cuda")
        result = whisper.transcribe(model, audio, language=lang)

        # Get result
        json_object = json.dumps(result, indent = 2, ensure_ascii = False)
        json_file_name = "./output/" + model_ver + "_" + file_name + ".json"

        # Write result to json file
        with open(json_file_name, "w", encoding="utf-8") as outfile:
            outfile.write(json_object)
        return "Done"
    except Exception as e:
        # Print error and continue
        print('File', file_path, 'has error', e)
        return "Error" + file_path


    # Print the progress
    print('Done writing json file for:', file_path)
    stop = timeit.default_timer()
    print('Running time: ', stop - start)

if __name__ == "__main__":
    file_path = str(sys.argv[1])
    model_ver = str(sys.argv[2])
    lang = str(sys.argv[3])
    audio_transcribe(file_path, model_ver, lang)