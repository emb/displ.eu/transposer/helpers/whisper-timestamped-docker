FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /code

RUN apt-get update
RUN apt-get install -y python3 python3-pip wget
RUN apt install -y git
RUN apt-get install -y ffmpeg
# RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
# RUN mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
# RUN wget https://developer.download.nvidia.com/compute/cuda/11.8.0/local_installers/cuda-repo-ubuntu2004-11-8-local_11.8.0-520.61.05-1_amd64.deb
# RUN dpkg -i cuda-repo-ubuntu2004-11-8-local_11.8.0-520.61.05-1_amd64.deb
# RUN cp /var/cuda-repo-ubuntu2004-11-8-local/cuda-*-keyring.gpg /usr/share/keyrings/
# RUN apt-get update
# RUN apt-get -y install cuda
# RUN apt-get install nvidia-container-runtime

# RUN nividia-smi

COPY ./requirements.txt /code/requirements.txt

RUN pip install -r /code/requirements.txt

COPY ./app /code/app
RUN mkdir ./output

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
# ENTRYPOINT [ "python3" ]

