<h1 align="center">Running Whisper-timestamped on docker</h1>
This is a simple script to run Whisper-timestamped on docker with GPU. Original repository in: https://github.com/linto-ai/whisper-timestamped



## Installation and usage
- Copy all files to your www directory, e.g. /var/www/whisper

- Build image
```sh
docker build -t whisper-gpu .  
```

- Create volume to contain files
```sh
docker volume create whisper-gpu-volume
```

- Run container with GPU and expose port
```sh
docker run -d -v whisper-gpu-volume:/shared-volume --gpus all --name whisper-gpu-container whisper-gpu
```

- Copy files from host machine to whisper's input directory
```sh
docker cp file/path/file_name.mp3 /var/www/whisper/input/
```
Note: Replace "file/path/file_name.mp3" with the actual file path

- Run script
```sh
docker compose -f /var/www/whisper/docker-compose.yml run --rm whisper-cba python3 app/script_whisper_word_timestamp.py /code/input/myfile.mp3 medium de
```

Note: Available Whisper version are "tiny", "base", "small", "medium", "large", multiple languages are allowed for transcription including "de": german, "en": english. For more details please refer to https://github.com/openai/whisper

- Copy output to host machine
The generated JSON transcript file will follow the convention: {model}_{original_filename}.json
```sh
docker cp /var/www/whisper/output/medium_myfile.json output/directory
```
Note: Replace "output/directory" with the actual file path
